#ifndef __TAN_BASE_H__
#define __TAN_BASE_H__

/**
 * \file Include bunch of basic utilities
 * */

#include <fmt/core.h>
#include "base/error.h"
#include "base/macro.h"
#include "base/container.h"

#endif
